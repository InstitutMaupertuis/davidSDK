[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# General information
- This is a modified copy of the [David-Laserscanner](http://david-3d.com) SDK. It allows the SDK to be used under GNU/Linux and with CMake.
- You need a [DAVID ENTERPRISE/SDK ](http://david-3d.com/en/products/david-sdk) software copy in order to use the SDK.
- The lastest versions of the SDK can be downloaded [here](http://www.david-3d.com/en/support/downloads).
- For licensing, please read the [license](LICENSE.txt) file.
- A [change log](ChangeLog.txt) is available.

# Documentation
- The documentation of the SDK can be found [here](http://docs.david-3d.com/sdk/en/index.html). (check the version number!).
- A tutorial on how to use the davidSDK with the Point Cloud Library is available [here](http://www.pointclouds.org/documentation/tutorials/davidsdk.php).

# Compile and install davidSDK
Clone, configure and compile:
```bash
mkdir davidSDK
cd davidSDK
git clone https://gitlab.com/InstitutMaupertuis/davidSDK.git src
mkdir build
cd build
cmake ../src
make -j2
```

Install:
```
sudo make -j2 install
```

# Example project using the davidSDK

```
.
├── build
└── src
    ├── CMakeLists.txt
    └── david_sls2.cpp
```

`CMakeLists.txt`
```cmake
cmake_minimum_required(VERSION 2.8)
project(davidSDK_test)

find_package(davidSDK 1.5.2 REQUIRED)
include_directories (${davidSDK_INCLUDE_DIRS})
link_directories    (${davidSDK_LIBRARY_DIRS})
add_definitions     (${davidSDK_DEFINITIONS})

add_executable (david_sls2 david_sls2.cpp)
target_link_libraries (david_sls2 davidSDK ${davidSDK_LIBRARIES})
```

`david_sls2.cpp`
```cpp
#include "david.h"

int main(int argc, char *argv[])
{
  try
  {
    david::Client david;
    if (argc == 2)
      david.Connect(argv[1]);
    else
      david.Connect();

    david.sls().Calibrate(120.0);
    david.sls().Scan();
  }
  catch (david::Exception& e)
  {
    e.PrintError();
  }

  return 0;
}
```

Configure and compile:
```bash
cd build
cmake ../src -DdavidSDK_DIR=/usr/local/lib/CMake/davidSDK/ # Default cmake path
make
./david_sls2 
```
